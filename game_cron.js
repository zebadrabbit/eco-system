var CronJob = require('cron').CronJob;
const mongoose = require('mongoose');
const bluebird = require('bluebird');

// set up mongoose
mongooseDb = 'mongodb://192.168.1.202/eco-system';
mongoose.Promise = bluebird;
mongoose.connect(mongooseDb, {});

var Helpers = require('./helpers');
var Games = require('./models/game');

// Set a cronjob to check for game updates
new CronJob('1 * * * * *', function() {
//new CronJob('* * * * * *', function() {

    Games.find({ 'in_progress': 0 })
    .exec(function(err, games) {
        console.log('There are ' + games.length + ' games waiting.');
        for (var i = 0; i <= games.length - 1; i++) {
            if (Helpers.checkGameReady(games[i])) {
                console.log('1 game ready');
                games[i].in_progress = 1;
                games[i].current_round = 1;
                games[i].save();
            }
        }
        
    });

    Games.find({ 'in_progress': 1 })
    .exec(function(err, games) {
        console.log('There are ' + games.length + ' games running.');
        for (i = 0; i <= games.length - 1; i++) {
            console.log('Game ' + i + ': current round: ' + games[i].current_round);
        }
    });

}, null, true, 'America/Chicago');
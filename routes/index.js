var express = require('express');
var router = express.Router();

var passport = require('../app').passport;
var User = require('../models/modelUser');


router.get('/', function(req, res, next) {
  res.render('index.html', { user: req.user });
});

router.get('/about', function(req, res, next) {
  res.render('about.html', { user: req.user });
});


// login get/post
router.get('/login', function(req, res, next) {
  res.render('login.html');
});

router.post('/login', function(req,res) {

  passport.authenticate('local-login', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })(req,res);

});

// sign-up get/post
router.get('/signup', function(req, res, next) {
  res.render('signup.html');
});

router.post('/signup', function(req, res, next) {

  passport.authenticate('local-signup', {
    successRedirect: '/',
    failureRedirect: '/signup',
    failureFlash: true
  })(req,res);

})


module.exports = router;

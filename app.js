// helmet fixes a lot of common exploits from the get-go
const helmet = require('helmet');
// compress all data transmitted
const compression = require('compression');

//-----------------------------------------------------------//

const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const nunjucks = require('nunjucks');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const bluebird = require('bluebird');
const assert = require('assert');
const Configstore = require('configstore');
const pkg = require('./package.json');

const app = express();

//-----------------------------------------------------------//

// creates a configuration under ~/.config/configstore/eco-system
// good place to keep settings for the game
const conf = new Configstore(pkg.name);
conf.set('awesome', true)

//-----------------------------------------------------------//

// html view engine
nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true
});

// set up the http engine
app.use(morgan('dev'));
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//-----------------------------------------------------------//

// set up mongoose
mongooseDb = 'mongodb://192.168.1.202/eco-system';
mongoose.Promise = bluebird;
mongoose.connect(mongooseDb, {});

// store session data into the mongo database
const store = new MongoDBStore({
  uri: mongooseDb,
  collection: 'sessions'
});

store.on('error', function(error) {
  assert.ifError(error);
  assert.ok(false);
});

//-----------------------------------------------------------//

// set up passport and passport-local
app.use(session({
	secret: 'oi4f6g5k0y5uwyx0051he5895', // change this!
    resave: true,
    saveUninitialized: true,
	store: store,
	cookie: { 
		maxAge: 1000 * 60 * 60 * 24 * 7
	}
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); 

// make the passport module available to login and signup
require('./models/passport')(passport);
module.exports.passport = passport;

//-----------------------------------------------------------//

// routes
app.use('/', require('./routes/index'));
app.use('/account', require('./routes/account'));
app.use('/admin', require('./routes/admin'));
app.use('/messaging', require('./routes/messaging'));
app.use('/game', require('./routes/game'));

// basic logout function, destroys cookie data
app.use('/logout', function(req, res, next) {
  req.logout()
  res.redirect('/');
});

//-----------------------------------------------------------//

// Error handling
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.render('error.html', {error: err.status});
});

module.exports = app;

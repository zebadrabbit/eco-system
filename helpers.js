var Business = require('./models/business');

module.exports.getEmptySlots = function (game) {

    available = [];

    if (!game.manufacturer) {
        available.push('Manufacturer');
    }
    
    if (!game.distributor) {
        available.push('Distributor');
    }
    
    if (!game.wholesaler) {
        available.push('Wholesaler');
    }
    
    if (!game.retailer) {
        available.push('Retailer');
    }
    

    return available;

};

module.exports.playerInGame = function(game, user) {

    if (game.manufacturer) {
        if (game.manufacturer.players._id.equals(user._id)) { 
            return true; 
        }
    }

    if (game.distributor) {
        if (game.distributor.players._id.equals(user._id)) { 
            return true; 
        }
    }

    if (game.wholesaler) {
        if (game.wholesaler.players._id.equals(user._id)) { 
            return true; 
        }
    }

    if (game.retailer) {
        if (game.retailer.players._id.equals(user._id)) { 
            return true; 
        }
    }

    return false;

}

module.exports.checkGameReady = function(game) {

    var cnt = 0;
    if (game.manufacturer) {
        cnt++;
    }

    if (game.distributor) {
        cnt++;
    }

    if (game.wholesaler) {
        cnt++;
    }

    if (game.retailer) {
        cnt++;
    }

    if (cnt >= 4) { 
        return true;
    } 

    return false;
    
}

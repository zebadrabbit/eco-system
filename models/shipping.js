var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var shippingScehma = mongoose.Schema({

    fromBusiness: {
        type: Schema.ObjectId,
        ref: 'business'
    },
    toBusiness: {
        type: Schema.ObjectId,
        ref: 'business'
    },
    // how many we're sending
    stock: Number,
    // when we sent them
    starting_round: Number,
    // delay caused by anomally
    additional_delay: Number

});

module.exports = mongoose.model('Shipping', shippingScehma);


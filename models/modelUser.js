var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var userScehma = mongoose.Schema({

    // global user options
    password: String,
    email: String,

    // groups
    business: Number,
    accounting: Number,
    security: Number,
    //special groups
    admin: Number,
    captain: Number,

});


userScehma.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}

userScehma.methods.compareHash = function(password) {
  return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('User', userScehma);
